import Image from "next/image";
import styles from "@/app/styles/option.module.scss";

const Option = (option) => {
  return (
    <>
      {option.image && (
        <Image
          width={50}
          height={50}
          src={option.image.url}
          alt={option.image.alt}
          className={styles.image}
        />
      )}
      <h4>{option.text}</h4>
    </>
  );
};
export default Option;
