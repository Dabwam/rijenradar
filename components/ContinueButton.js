import styles from "@/app/styles/button.module.scss";

const ContinueButton = () => {
  let buttonText = "verder";
  return (
    <div className={styles.continueButtonHolder}>
      <button className={styles.continueButton}>{buttonText}</button>
    </div>
  );
};
export default ContinueButton;
