"use client";
import React, { useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import styles from "@/app/styles/page.module.scss";

export default function CalendarHolder() {
  const [value, onChange] = useState(new Date());
  let headerText = "Plan je bezoek";
  let secondHeaderText = "Wanneer wil je langskomen?";

  return (
    <div className={styles.calendarHolder}>
      <h2>{headerText}</h2>
      <h4>{secondHeaderText}</h4>
      <Calendar onChange={onChange} value={value} />
    </div>
  );
}
