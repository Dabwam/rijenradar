import OptionsList from "./OptionsList";
import onePerson from "../public/icons/user.svg";
import twoPersons from "../public/icons/users.svg";
import threePersons from "../public/icons/users-three.svg";
import fourPersons from "../public/icons/users-four.svg";

export default function VisitorCountSelection() {
  let headerText = "Personen";
  let secondHeaderText = "Met hoeveel personen ben je?";
  let options = [
    {
      text: "1",
      image: {
        url: onePerson,
        alt: "one person icon",
      },
      value: 1,
    },
    {
      text: "2",
      image: {
        url: twoPersons,
        alt: "two persons icon",
      },
      value: 2,
    },
    {
      text: "3",
      image: {
        url: threePersons,
        alt: "three persons icon",
      },
      value: 3,
    },
    {
      text: "4",
      image: {
        url: fourPersons,
        alt: "four persons icon",
      },
      value: 4,
    },
  ];
  return (
    <div className="visitor-count-selection">
      <h2>{headerText}</h2>
      <h4>{secondHeaderText}</h4>
      {OptionsList(options)}
    </div>
  );
}
