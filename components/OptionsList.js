import Option from "./Option";
import optionStyles from "@/app/styles//option.module.scss";
import wrapperStyles from "@/app/styles//optionslist.module.scss";
import { useEffect, useState } from "react";

const OptionsList = (options) => {
  if (!options || options.length === 0) return <></>;

  const KeyGenerator = (pre) => {
    return `${pre}_${new Date().getTime()}`;
  };

  const optionsArray = Object.values(options);

  const [listValue, setListValue] = useState(0);

  useEffect(() => {}, [listValue]);

  const scrollRight = () => {};

  const handleClick = (value) => {
    setListValue(value);
  };

  return (
    <div>
      <div className={wrapperStyles.optionsWrapper}>
        {optionsArray &&
          optionsArray.map((option, index) => {
            {
              return (
                <button
                  key={KeyGenerator(index)}
                  className={
                    option.value == listValue
                      ? optionStyles.optionSelected
                      : optionStyles.option
                  }
                  onClick={(e) => handleClick(option.value)}
                  value={option.value}
                >
                  {Option(option, index)}
                </button>
              );
            }
          })}
      </div>
      <button className={wrapperStyles.scrollRight} onClick={scrollRight()}>
        {" "}
        &#10148;{" "}
      </button>
    </div>
  );
};
export default OptionsList;
