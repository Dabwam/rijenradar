import OptionsList from "./OptionsList";
import styles from "@/app/styles/page.module.scss";

export default function TimeSlotSelection() {
  let headerText = "Selecteer een tijdsslot";
  let secondHeaderText = "Hoe laat wil je ongeveer langskomen?";
  let options = [
    {
      text: "09:00 tot 10:00",
      value: 1,
    },
    {
      text: "10:00 tot 11:00",
      value: 2,
    },
    {
      text: "11:00 tot 12:00",
      value: 3,
    },
    {
      text: "12:00 tot 13:00",
      value: 4,
    },
  ];
  return (
    <div className={styles.timeSlotSelection}>
      <h2>{headerText}</h2>
      <h4>{secondHeaderText}</h4>
      {OptionsList(options)}
    </div>
  );
}
