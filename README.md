# Rijenradar

## Notities

De repository heet Rijenradar, maar het gedeelte wat af is, heeft BezoekBoeker. Het is gedeeltelijk af, zie volgende stappen om te zien wat er nog moet gebeuren.

## Volgende stappen (niet op volgorde van belang)

1. Scrollen forceren door op de knop te klikken in OptionsList.js
2. Scrollbar weghalen (niet door middel van hidden, dan werkt touch ook niet)
3. SVGs wit maken
4. Home opdelen, zodat er genavigeerd kan worden naar de volgende stap: het controleren van gegevens
5. SummaryPage maken
6. Object opsturen met API naar ???
7. Calendar scss aanpassen
8. Golven bovenin maken
9. Checkmark toevoegen aan selectie
