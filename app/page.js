"use client";
import Image from "next/image";
import styles from "@/app/styles/page.module.scss";
import VisitorCountSelection from "@/components/VisitorCountSelection";
import TimeSlotSelection from "@/components/TimeSlotSelection";
import ContinueButton from "@/components/ContinueButton";
import HeaderImage from "../public/images/iStock-503590178.jpg";
import dynamic from "next/dynamic";

export default function Home() {
  const CalendarWithoutSSR = dynamic(
    () => import("@/components/CalendarHolder"),
    {
      ssr: false,
    }
  );

  const headerText = "Reserveer een bezoek bij een locatie";
  const imageAlt = "two women laughing";

  return (
    <main className={styles.main}>
      <Image className={styles.headerImage} src={HeaderImage} alt={imageAlt} />
      <div className={styles.moduleWrapper}>
        <h1>{headerText}</h1>
        <VisitorCountSelection />
        <CalendarWithoutSSR />
        <TimeSlotSelection />
      </div>
      <ContinueButton />
    </main>
  );
}
