import Nav from "@/components/Nav";
import { API_URL } from "@/utils/urls";
import axios from "axios";

async function getData(slug) {
  try {
    const response = await axios.get(`${API_URL}/pages/?slug=${slug}`);
    const result = response.data.data[0].attributes;
    return result;
  } catch (error) {
    //do somethign with the error
  }
}

export default async function Page(slug) {
  const data = await getData(slug);
  return (
    <main>
      <h1>{data?.Title}</h1>
      <p>{data?.Content[0].children[0].text}</p>
    </main>
  );
}
