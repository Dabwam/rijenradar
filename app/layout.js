import "./globals.scss";
import localFont from "next/font/local";

// Font files can be colocated inside of `app`
const palanquin = localFont({
  src: "../public/fonts/Palaquin/Palanquin-Medium.ttf",
  display: "swap",
});

export const metadata = {
  title: "Bezoekerboeker",
  description: "Boek je bezoek!",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={palanquin.className}>{children}</body>
    </html>
  );
}
